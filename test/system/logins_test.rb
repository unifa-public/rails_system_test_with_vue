require 'application_system_test_case'

class LoginsTest < ApplicationSystemTestCase
  test 'login process' do
    # テストユーザーを作成。テスト終了後に自動で削除される。
    user = ::User.create!(uid: 'uid', password: 'password', name: 'name')

    visit root_url

    assert_selector 'h1', text: 'ログイン画面'

    # ユーザーIDが空
    fill_in('uid', with: '')
    fill_in('password', with: 'password')
    click_button('ログインする')
    assert_text 'ユーザーIDを入力してください。'

    # パスワードが空
    fill_in('uid', with: 'uid')
    fill_in('password', with: '')
    click_button('ログインする')
    assert_text 'パスワードを入力してください。'

    # ユーザーIDが間違っている
    fill_in('uid', with: 'wrong_uid')
    fill_in('password', with: 'password')
    click_button('ログインする')
    assert_text 'ユーザーID、またはパスワードに間違いがあります。'

    # パスワードが間違っている
    fill_in('uid', with: 'uid')
    fill_in('password', with: 'wrong_password')
    click_button('ログインする')
    assert_text 'ユーザーID、またはパスワードに間違いがあります。'

    # 正しいユーザーIDとパスワード
    fill_in('uid', with: user.uid)
    fill_in('password', with: user.password)
    click_button('ログインする')

    # ログイン後の画面の確認
    assert_current_path('/mypage')
    assert_selector 'h1', text: 'マイページ'
    assert_text user.name
  end
end
