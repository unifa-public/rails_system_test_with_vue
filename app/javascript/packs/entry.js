/* eslint no-console: 0 */

import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import routes from './pages/routes'

const router = new VueRouter({
  mode: 'history',
  routes
});

import axios from 'axios';

Vue.prototype.$axios = axios.create({
  withCredentials: true,
})

import App from './App.vue'

document.addEventListener("DOMContentLoaded", () => {
  const el = document.getElementById("app");
  const app = new Vue({
    el,
    router,
    render: h => h(App)
  });
});
