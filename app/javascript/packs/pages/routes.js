import LoginPage from './Login'
import MyPage from './MyPage'

const routes = [
  {
    path: '/',
    name: 'login',
    component: LoginPage,
  },
  {
    path: '/mypage',
    name: 'mypage',
    component: MyPage,
  }
];

export default routes;