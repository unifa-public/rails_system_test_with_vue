module Api
  class UsersController < ::Api::ApplicationController
    before_action :abort_if_not_authenticated

    def me
      render json: { user: { id: @user.id, name: @user.name } }
    end
  end
end