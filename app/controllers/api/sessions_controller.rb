module Api
  class SessionsController < ::Api::ApplicationController
    def create
      form = ::Api::Sessions::CreateForm.new(session_params)
      if form.invalid?
        render json: { errors: form.errors.full_messages }, status: 400
        return
      end

      user = ::User.find_by(uid: form.uid)
      if user && user.authenticate(form.password)
        reset_session
        session[:user_id] = user.id

        render json: { user: { id: user.id, name: user.name } }, status: 201
      else
        render json: { errors: ['ユーザーID、またはパスワードに間違いがあります。'] }, status: 400
      end
    end

    private

    def session_params
      params.require(:session).permit(:uid, :password)
    end
  end
end