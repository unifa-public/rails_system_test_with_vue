module Api
  class ApplicationController < ::ApplicationController
    skip_before_action :verify_authenticity_token

    def abort_if_not_authenticated
      @user = ::User.find_by(id: session[:user_id])
      if @user.nil?
        render json: { errors: ['ログインが必要です。'] }, status: 401
      end
    end
  end
end
