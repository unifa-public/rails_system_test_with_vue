class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :uid, null: false, limit: 100
      t.string :password_digest, null: false, limit: 255
      t.string :name, null: false, limit: 255

      t.timestamps
    end

    add_index :users, :uid, unique: true
  end
end
