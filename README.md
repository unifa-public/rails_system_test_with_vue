# rails_system_test_with_vue

Rails + Webpacker + Vue.jsの構成でSystem Testを動かしてみる のテスト用アプリケーションです。

## 必要な環境

* Ruby
* bundler
* node
* yarn
* sqlite3
* Google Chrome
* chromedriver

## 実行手順

```bash
git clone https://bitbucket.org/unifa-public/rails_system_test_with_vue.git
cd rails_system_test_with_vue

bundle install
yarn install

rails db:create
rails db:migrate
rails db:seed

foreman start

# Open http://localhost:3000
# ユーザーID: uid
# パスワード: password
```

システムテストの実行

```bash
rails test:system
```
