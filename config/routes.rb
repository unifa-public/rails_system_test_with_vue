Rails.application.routes.draw do
  root to: 'pages#index'

  namespace :api do
    resource :session, only: [:create]

    resources :users, only: [] do
      collection do
        get 'me'
      end
    end
  end
end
